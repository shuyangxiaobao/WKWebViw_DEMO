//
//  AppDelegate.h
//  WkWebView_Demo
//
//  Created by 戈强宝 on 2017/6/16.
//  Copyright © 2017年 戈强宝. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

