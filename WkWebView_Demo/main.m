//
//  main.m
//  WkWebView_Demo
//
//  Created by 戈强宝 on 2017/6/16.
//  Copyright © 2017年 戈强宝. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
