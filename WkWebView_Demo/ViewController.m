//
//  ViewController.m
//  WkWebView_Demo
//
//  Created by 戈强宝 on 2017/6/16.
//  Copyright © 2017年 戈强宝. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WKWebView.h>
#import <WebKit/WebKit.h>


#define kNEWTaskSuccessNotification @"NEWTaskSuccessNotification"
#if 1   //指挥中心正式接口
#define KCONTROLCENTERURL @"http://xpsgbak.weilian.cn:33122/NVRCT"
#define KCONTROLCENTERIMURL @"http://xpsgbak.weilian.cn:33088"
#else //指挥中心测试接口
#define KCONTROLCENTERURL @"http://172.19.6.104:8091/NVRCT"
#define KCONTROLCENTERIMURL @"http://119.23.25.233:8083"
#endif
#define kScreenWidth                        [[UIScreen mainScreen] bounds].size.width

@interface ViewController ()<WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler>
@property (strong, nonatomic)  WKWebView *wkWebView;
@property (nonatomic,strong) UIProgressView *progressView;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //跳进appstore
    if (1) {
        NSString *str = @"http://itunes.apple.com/cn/app/爱搭配-穿衣搭配时尚潮流女神必备/id869240848?mt=8";
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *URL = [NSURL URLWithString:str];
        
        [[UIApplication sharedApplication] openURL:URL];
        return;

    }
      [self initwebView];
    // Do any additional setup after loading the view, typically from a nib.
}


/**
 初始化WKWebView
 */
-(void)initwebView{
    //设置网页的配置文件
    WKWebViewConfiguration * Configuration = [[WKWebViewConfiguration alloc]init];
    //允许视频播放
    Configuration.allowsAirPlayForMediaPlayback = YES;
    
    // 允许在线播放
    Configuration.allowsInlineMediaPlayback = YES;
    // 允许可以与网页交互，选择视图
    Configuration.selectionGranularity = YES;
    // web内容处理池
    Configuration.processPool = [[WKProcessPool alloc] init];
    //自定义配置,一般用于 js调用oc方法(OC拦截URL中的数据做自定义操作)
    WKUserContentController * UserContentController = [[WKUserContentController alloc]init];
    // 添加消息处理，注意：self指代的对象需要遵守WKScriptMessageHandler协议，结束时需要移除
    [UserContentController addScriptMessageHandler:self name:@"jsCallOC"];
    [UserContentController addScriptMessageHandler:self name:@"commandCenterVideoShow"];
    [UserContentController addScriptMessageHandler:self name:@"commandCenterSendcommandIdcommandName"];
    // 是否支持记忆读取
    Configuration.suppressesIncrementalRendering = YES;
    // 允许用户更改网页的设置
    Configuration.userContentController = UserContentController;
    _wkWebView = [[WKWebView alloc] initWithFrame:self.view.bounds  configuration:Configuration];
    _wkWebView.UIDelegate = self;
    _wkWebView.navigationDelegate = self;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/commandCenterOA/command-home/sales_rb.html",KCONTROLCENTERURL]];
    //    NSURL *url = [NSURL URLWithString:@"http://172.19.5.177:9090/commandCenterOA/command-home/sales_rb.html"];
    url = [NSURL URLWithString:@"http://www.baidu.com"] ;
    //    NSString *webPath = [[NSBundle  mainBundle] pathForResource:@"echarts" ofType:@"html"];//获取文件路径
    //    NSURL *webURL = [NSURL  fileURLWithPath:webPath];//通过文件路径字符串设置URL
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    
    [_wkWebView loadRequest:urlRequest]; // 加载页面
    
    [self.view addSubview:_wkWebView];
//    [_wkWebView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        
//        make.top.right.bottom.left.equalTo(self.view);
//        
//    }];
        [_wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
        [_wkWebView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    
        _progressView = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
        // 设置进度条的色彩
        [_progressView setTrackTintColor:[UIColor colorWithRed:240.0/255 green:240.0/255 blue:240.0/255 alpha:1.0]];
        _progressView.progressTintColor = [UIColor greenColor];
    _progressView.frame = CGRectMake(0, 20, kScreenWidth,5);
        [self.view addSubview:_progressView];
    
    NSMutableDictionary *cookieDic = [NSMutableDictionary dictionary];
    NSMutableString *cookieValue = [NSMutableString stringWithFormat:@""];
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [cookieJar cookies]) {
        [cookieDic setObject:cookie.value forKey:cookie.name];
    }
  
    
    
    // cookie重复，先放到字典进行去重，再进行拼接
    for (NSString *key in cookieDic) {
        NSString *appendString = [NSString stringWithFormat:@"%@=%@;", key, [cookieDic valueForKey:key]];
        [cookieValue appendString:appendString];
    }
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://www.baidu.com"]];
    [request addValue:cookieValue forHTTPHeaderField:@"Cookie"];
    NSLog(@"添加cookie");
    [self.wkWebView loadRequest:request];
    
    
}

//KVO监听进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {

    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))] && object == _wkWebView) {
        [_progressView setAlpha:1.0f];
        BOOL animated = _wkWebView.estimatedProgress > _progressView.progress;
        [_progressView setProgress:_wkWebView.estimatedProgress animated:animated];

        // Once complete, fade out UIProgressView
        if(_wkWebView.estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.3f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                [_progressView setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [_progressView setProgress:0.0f animated:NO];
            }];
        }
    }else if ([keyPath isEqualToString:@"title"]) {
        self.title = _wkWebView.title;
    }else if ([keyPath isEqualToString:@"contentSize"]) {


    }

}


#pragma mark ================ WKNavigationDelegate ================

//这个是网页加载完成，导航的变化
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    /*
     主意：这个方法是当网页的内容全部显示（网页内的所有图片必须都正常显示）的时候调用（不是出现的时候就调用），，否则不显示，或则部分显示时这个方法就不调用。
     */
    
    // 获取加载网页的标题
    self.title = _wkWebView.title;
    NSString *jsStr;
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    //    if (_commandId) {
    //        [NSString stringWithFormat:@"jsCallOCChangeCommand('%ld','%@','%@')",(long)_compId,_commandId,kUserAccountModelSingle.userIdStr];
    //    }else{
    //        [NSString stringWithFormat:@"jsCallOCChangeCommand('%ld','%@','%@')",(long)_compId,@"0",kUserAccountModelSingle.userIdStr];
    //
    //    }
    
    
    //    NSString *jsStr = @"IOSAndrSendParams6('123123123')";
    NSLog(@"%@",jsStr);
    [_wkWebView evaluateJavaScript:jsStr completionHandler:^(id _Nullable data, NSError * _Nullable error) {
        if (error) {
            NSLog(@"错误:%@", error.localizedDescription);
        }
    }];
    
}

//开始加载
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    //开始加载的时候，让加载进度条显示
    //    _progressView.hidden = NO;
}

//内容返回时调用
-(void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{}

//服务器请求跳转的时候调用
-(void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{}

//服务器开始请求的时候调用
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    decisionHandler(WKNavigationActionPolicyAllow);
}

// 内容加载失败时候调用
-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    NSLog(@"页面加载超时");
}
//跳转失败的时候调用
-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{}

//进度条
-(void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{}

// 获取js 里面的提示
-(void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }]];
    
    [self presentViewController:alert animated:YES completion:NULL];
}

// js 信息的交流
-(void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(NO);
    }]];
    [self presentViewController:alert animated:YES completion:NULL];
}

// 交互。可输入的文本。
-(void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"textinput" message:@"JS调用输入框" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.textColor = [UIColor redColor];
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler([[alert.textFields lastObject] text]);
    }]];
    
    [self presentViewController:alert animated:YES completion:NULL];
    
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    NSLog(@"方法名:%@", message.name);
    NSLog(@"参数:%@", message.body);
    // 方法名
    NSString *methods = [NSString stringWithFormat:@"%@:", message.name];
    SEL selector = NSSelectorFromString(methods);
    // 调用方法
    if ([self respondsToSelector:selector]) {
        [self performSelector:selector withObject:message.body];
    } else {
        NSLog(@"未实行方法：%@", methods);
    }
}


#pragma mark js调OC
- (void)jsCallOC:(id)body {
    //    if ([body isKindOfClass:[NSDictionary class]]) {
    //        NSDictionary *dict = (NSDictionary *)body;
    //        // oc调用js代码
    ////        NSString *jsStr = [NSString stringWithFormat:@"ocCallJS('%@')", [dict objectForKey:@"data"]];
    ////        [_wkWebView evaluateJavaScript:jsStr completionHandler:^(id _Nullable data, NSError * _Nullable error) {
    ////            if (error) {
    ////                NSLog(@"错误:%@", error.localizedDescription);
    ////            }
    ////        }];
    //    }
    //    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    CCCenterRoomController * controller = [storyBoard instantiateViewControllerWithIdentifier:@"CCCenterRoomController"];
    //    controller.compId = _compId;
    //    controller.commandId = _commandId;
    //    controller.commandName = _commandName;
    //    [self.navigationController pushViewController:controller animated:YES];
}


/**
 h5交互接口，调用视频接口
 
 @param body <#body description#>
 */
-(void)commandCenterVideoShow:(id)body{
    
    if ([body isKindOfClass:[NSDictionary class]]) {
        
        
        
        
        
        NSDictionary *dict = (NSDictionary *)body;
        
        
         }
}

-(void)commandCenterSendcommandIdcommandName:(id)body{
    
    NSLog(@"获取参数:%@", body);
    if ([body isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *dict = (NSDictionary *)body;
        
          }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
